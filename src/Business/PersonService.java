package Business;

import java.util.ArrayList;

import DataModels.Person;

public class PersonService {
	public ArrayList<Person> Persons;

	public PersonService() {
		Persons = new ArrayList<Person>();
		for (int i = 0; i < 4; i++)
			Persons.add(new Person());
	}

	public PersonService(ArrayList<Person> p) {
		Persons = p;
	}

	public Person Get(int index) {
		return Persons.get(index);
	}

	public ArrayList<Person> GetAll() {
		return Persons;
	}

	public void Delete(int index) {
		Persons.set(index, new Person());
	}

	public void Add(int index, Person p) {
		Persons.set(index, p);
	}

	public int nbInitialized() {
		int nbIn = 0;
		for (int i = 0; i < 4; i++) {
			if (Persons.get(i).isInitialized) {
				nbIn++;
			}
		}
		return nbIn;
	}

}
