package Views;

import javax.swing.JFrame;
import javax.swing.JTextArea;

import Business.PersonService;

public class resultView {

	JTextArea textArea;
	String line = "";

	public resultView(PersonService result) {
		JFrame frame = new JFrame("Liste des inscrits");
		textArea = new JTextArea();
		textArea.setEditable(false);

		for (int i = 0; i < 4; i++) {
			if (result.Get(i).isInitialized) {
				textArea.append(result.Get(i).FirstName + "   " + result.Get(i).LastName + "  "
						+ result.Get(i).PhoneNumber + "\n");
				// textArea.setCaretPosition(textArea.getDocument().getLength());
			} else {
				textArea.append("Empty" + "\n");
			}
		}
		frame.getContentPane().add(textArea);

		frame.setSize(300, 200);
		frame.setLocation(400, 400);
		frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		frame.setVisible(true);

	}

}