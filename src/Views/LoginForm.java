package Views;

import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Arrays;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPasswordField;

public class LoginForm extends JDialog implements ActionListener {
	private static final long serialVersionUID = 1L;

	public boolean isOK = false;
	Container container = getContentPane();
	JLabel passwordLabel = new JLabel("MOT DE PASSE");
	JPasswordField passwordField = new JPasswordField();
	JButton loginButton = new JButton("OK");

	public LoginForm() {
		setLayoutManager();
		setLocationAndSize();
		addComponentsToContainer();
		addActionEvent();

	}

	public void setLayoutManager() {
		container.setLayout(null);
	}

	public void setLocationAndSize() {
		passwordLabel.setBounds(10, 10, 120, 30);
		passwordField.setBounds(130, 10, 120, 30);
		loginButton.setBounds(60, 60, 100, 30);

	}

	public void addComponentsToContainer() {
		container.add(passwordLabel);
		container.add(passwordField);
		container.add(loginButton);
	}

	public void addActionEvent() {
		loginButton.addActionListener(this);
	}

	private static boolean isPasswordCorrect(char[] input) {
		boolean isCorrect = true;
		char[] correctPassword = { '1', '2', '3', '4', '5' };

		if (input.length != correctPassword.length) {
			isCorrect = false;
		} else {
			isCorrect = Arrays.equals(input, correctPassword);
		}

		// Zero out the password.
		Arrays.fill(correctPassword, '0');

		return isCorrect;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		// Coding Part of LOGIN button
		if (e.getSource() == loginButton) {
			char[] pwdText;
			pwdText = passwordField.getPassword();
			if (isPasswordCorrect(pwdText)) {
				isOK = true;
				dispose();
				// JOptionPane.showMessageDialog(this, "Login Successful");
			} else {
				JOptionPane.showMessageDialog(this, "Mot de passe invalide");
			}

		}

	}

	public boolean run() {
		this.setTitle("Login Form");
		this.setBounds(300, 300, 300, 160);
		this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		this.setModal(true);
		this.setResizable(false);
		this.setVisible(true);
		return (isOK);
	}

}

//public class Login {
// public static void main(String[] a) {
//        LoginForm frame = new LoginForm();
//        frame.setTitle("Login Form");
//        frame.setVisible(true);
//        frame.setBounds(10, 10, 370, 600);
//        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
//        frame.setResizable(false);
//
//    }
//
//}