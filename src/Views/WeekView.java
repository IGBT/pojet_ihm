package Views;

import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.DateFormat;
import java.util.Calendar;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import org.json.JSONArray;
import org.json.JSONObject;

import Business.PersonService;

public class WeekView extends JPanel implements ActionListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String label;
	private String CBLabel;
	private int number;
	private String btn1 = "Disponible";
	private String btn2 = "Disponible";
	private String btn3 = "Disponible";
	private String btn4 = "Disponible";
	private PersonService service;

	private JButton addP1;
	private JButton addP2;
	private JButton addP3;
	private JButton addP4;

	private JButton delP1;
	private JButton delP2;
	private JButton delP3;
	private JButton delP4;

	private JButton exportBtn;

	/**
	 * Create the panel.
	 */
	public WeekView(int _number, PersonService _service) {
		number = _number;
		label = getDateOfWeek(_number);
		service = _service;
		CBLabel = new StringBuilder().append(getDateOfWeek(number)).append("   : ").append(service.nbInitialized())
				.append(" inscrit(s)").toString();
		GridBagLayout gridBagLayout = new GridBagLayout();
		gridBagLayout.columnWidths = new int[] { 225, 100, 0 };
		gridBagLayout.rowHeights = new int[] { 10, 50, 50, 50, 50, 50, 50, 50, 10 };
		setLayout(gridBagLayout);

		JLabel lblNewLabel = new JLabel(label);
		lblNewLabel.setBackground(Color.WHITE);
		lblNewLabel.setOpaque(true);
		lblNewLabel.setHorizontalAlignment(JLabel.CENTER);
		GridBagConstraints gbc_lblNewLabel = new GridBagConstraints();
		gbc_lblNewLabel.fill = GridBagConstraints.BOTH;
		gbc_lblNewLabel.gridwidth = 2;
		gbc_lblNewLabel.insets = new Insets(0, 0, 0, 0);
		gbc_lblNewLabel.gridx = 0;
		gbc_lblNewLabel.gridy = 1;
		add(lblNewLabel, gbc_lblNewLabel);

		addP1 = new JButton(btn1);
		if (service.Get(0).isInitialized) {
			addP1.setText(String.join(" ", service.Get(0).FirstName, service.Get(0).LastName));
			addP1.setBackground(Color.GREEN);
			addP1.setEnabled(false);
		}
		addP1.addActionListener(this);
		GridBagConstraints gbc_btnNewButton = new GridBagConstraints();
		gbc_btnNewButton.fill = GridBagConstraints.BOTH;
		gbc_btnNewButton.insets = new Insets(0, 0, 5, 5);
		gbc_btnNewButton.gridx = 0;
		gbc_btnNewButton.gridy = 2;
		add(addP1, gbc_btnNewButton);

		delP1 = new DeleteButton();
		if (service.Get(0).isInitialized) {
			delP1.setEnabled(true);
		}
		delP1.addActionListener(this);
		GridBagConstraints gbc_btnNewButton_5 = new GridBagConstraints();
		gbc_btnNewButton_5.fill = GridBagConstraints.VERTICAL;
		gbc_btnNewButton_5.insets = new Insets(0, 0, 5, 5);
		gbc_btnNewButton_5.gridx = 1;
		gbc_btnNewButton_5.gridy = 2;
		add(delP1, gbc_btnNewButton_5);

		addP2 = new JButton(btn2);
		if (service.Get(1).isInitialized) {
			addP2.setText(String.join(" ", service.Get(1).FirstName, service.Get(1).LastName));
			addP2.setBackground(Color.GREEN);
			addP2.setEnabled(false);
		}
		addP2.addActionListener(this);
		GridBagConstraints gbc_btnNewButton_1 = new GridBagConstraints();
		gbc_btnNewButton_1.fill = GridBagConstraints.BOTH;
		gbc_btnNewButton_1.insets = new Insets(0, 0, 5, 5);
		gbc_btnNewButton_1.gridx = 0;
		gbc_btnNewButton_1.gridy = 3;
		add(addP2, gbc_btnNewButton_1);

		delP2 = new DeleteButton();
		if (service.Get(1).isInitialized) {
			delP2.setEnabled(true);
		}
		delP2.addActionListener(this);
		GridBagConstraints gbc_btnNewButton_5_1 = new GridBagConstraints();
		gbc_btnNewButton_5_1.fill = GridBagConstraints.VERTICAL;
		gbc_btnNewButton_5_1.insets = new Insets(0, 0, 5, 5);
		gbc_btnNewButton_5_1.gridx = 1;
		gbc_btnNewButton_5_1.gridy = 3;
		add(delP2, gbc_btnNewButton_5_1);

		addP3 = new JButton(btn3);
		if (service.Get(2).isInitialized) {
			addP3.setText(String.join(" ", service.Get(2).FirstName, service.Get(2).LastName));
			addP3.setBackground(Color.GREEN);
			addP3.setEnabled(false);
		}
		addP3.addActionListener(this);
		GridBagConstraints gbc_btnNewButton_2 = new GridBagConstraints();
		gbc_btnNewButton_2.fill = GridBagConstraints.BOTH;
		gbc_btnNewButton_2.insets = new Insets(0, 0, 5, 5);
		gbc_btnNewButton_2.gridx = 0;
		gbc_btnNewButton_2.gridy = 4;
		add(addP3, gbc_btnNewButton_2);

		delP3 = new DeleteButton();
		if (service.Get(2).isInitialized) {
			delP3.setEnabled(true);
		}
		delP3.addActionListener(this);
		GridBagConstraints gbc_btnNewButton_5_2 = new GridBagConstraints();
		gbc_btnNewButton_5_2.fill = GridBagConstraints.VERTICAL;
		gbc_btnNewButton_5_2.insets = new Insets(0, 0, 5, 5);
		gbc_btnNewButton_5_2.gridx = 1;
		gbc_btnNewButton_5_2.gridy = 4;
		add(delP3, gbc_btnNewButton_5_2);

		addP4 = new JButton(btn4);
		if (service.Get(3).isInitialized) {
			addP4.setText(String.join(" ", service.Get(3).FirstName, service.Get(3).LastName));
			addP4.setBackground(Color.GREEN);
			addP4.setEnabled(false);
		}
		addP4.addActionListener(this);
		GridBagConstraints gbc_btnNewButton_3 = new GridBagConstraints();
		gbc_btnNewButton_3.fill = GridBagConstraints.BOTH;
		gbc_btnNewButton_3.insets = new Insets(0, 0, 5, 5);
		gbc_btnNewButton_3.gridx = 0;
		gbc_btnNewButton_3.gridy = 5;
		add(addP4, gbc_btnNewButton_3);

		delP4 = new DeleteButton();
		if (service.Get(3).isInitialized) {
			delP4.setEnabled(true);
		}
		delP4.addActionListener(this);
		GridBagConstraints gbc_btnNewButton_5_3 = new GridBagConstraints();
		gbc_btnNewButton_5_3.fill = GridBagConstraints.VERTICAL;
		gbc_btnNewButton_5_3.insets = new Insets(0, 0, 5, 5);
		gbc_btnNewButton_5_3.gridx = 1;
		gbc_btnNewButton_5_3.gridy = 5;
		add(delP4, gbc_btnNewButton_5_3);

		exportBtn = new JButton("Voir les coordonnées");
		exportBtn.addActionListener(this);
		GridBagConstraints gbc_exportBtn = new GridBagConstraints();
		gbc_exportBtn.fill = GridBagConstraints.BOTH;
		gbc_exportBtn.gridwidth = 2;
		gbc_exportBtn.insets = new Insets(0, 0, 0, 5);
		gbc_exportBtn.gridx = 0;
		gbc_exportBtn.gridy = 7;
		add(exportBtn, gbc_exportBtn);

	}

	public void actionPerformed(ActionEvent e) {
//		System.out.println(e.getSource().equals(addP1));
		PersonView dialog = null;

		if (e.getSource().equals(addP1)) {
			dialog = new PersonView(0, service);
			btn1 = dialog.run();
			if (!btn1.isBlank()) {
				addP1.setText(btn1);
				addP1.setBackground(Color.GREEN);
				addP1.setEnabled(false);
				delP1.setEnabled(true);
				this.setCBLabel(number, service);
			}
		}
		if (e.getSource().equals(addP2)) {
			dialog = new PersonView(1, service);
			btn2 = dialog.run();
			if (!btn2.isBlank()) {
				addP2.setText(btn2);
				addP2.setBackground(Color.GREEN);
				addP2.setEnabled(false);
				delP2.setEnabled(true);
				this.setCBLabel(number, service);

			}
		}
		if (e.getSource().equals(addP3)) {
			dialog = new PersonView(2, service);
			btn3 = dialog.run();
			if (!btn3.isBlank()) {
				addP3.setText(btn3);
				addP3.setBackground(Color.GREEN);
				addP3.setEnabled(false);
				delP3.setEnabled(true);
				this.setCBLabel(number, service);

			}
		}
		if (e.getSource().equals(addP4)) {

			dialog = new PersonView(3, service);
			btn4 = dialog.run();
			if (!btn4.isBlank()) {
				addP4.setText(btn4);
				addP4.setBackground(Color.GREEN);
				addP4.setEnabled(false);
				delP4.setEnabled(true);
				this.setCBLabel(number, service);

			}
		}
		if (e.getSource().equals(delP1)) {
			int res = JOptionPane.showConfirmDialog(this, "Etes-vous sûr(e) ?", "confirm-dialog",
					JOptionPane.YES_NO_OPTION);

			if (res == JOptionPane.YES_OPTION) {
				btn1 = "Disponible";
				service.Delete(0);
				addP1.setText(btn1);
				addP1.setBackground(null);
				addP1.setEnabled(true);
				delP1.setEnabled(false);
				this.setCBLabel(number, service);

			}

		}
		if (e.getSource().equals(delP2)) {
			int res = JOptionPane.showConfirmDialog(this, "Etes-vous sûr(e) ?", "confirm-dialog",
					JOptionPane.YES_NO_OPTION);

			if (res == JOptionPane.YES_OPTION) {
				service.Delete(1);
				btn2 = "Disponible";
				addP2.setText(btn2);
				addP2.setBackground(null);
				addP2.setEnabled(true);
				delP2.setEnabled(false);
				this.setCBLabel(number, service);
				repaint();

			}
		}
		if (e.getSource().equals(delP3)) {
			int res = JOptionPane.showConfirmDialog(this, "Etes-vous sûr(e) ?", "confirm-dialog",
					JOptionPane.YES_NO_OPTION);

			if (res == JOptionPane.YES_OPTION) {
				service.Delete(2);
				btn3 = "Disponible";
				addP3.setText(btn3);
				addP3.setBackground(null);
				addP3.setEnabled(true);
				delP3.setEnabled(false);
				this.setCBLabel(number, service);

			}
		}
		if (e.getSource().equals(delP4)) {
			int res = JOptionPane.showConfirmDialog(this, "Etes-vous sûr(e) ?", "confirm-dialog",
					JOptionPane.YES_NO_OPTION);

			if (res == JOptionPane.YES_OPTION) {
				service.Delete(3);
				btn4 = "Disponible";
				addP4.setText(btn4);
				addP4.setBackground(null);
				addP4.setEnabled(true);
				delP4.setEnabled(false);
				this.setCBLabel(number, service);

			}
		}
		if (e.getSource().equals(exportBtn)) {
			LoginForm lg = new LoginForm();
			if (lg.run() == true) {
				new resultView(service);
			}

		}

		// dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		// dialog.setVisible(true);
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	private String getDateOfWeek(int weekNb) {
		String mondayDate = "";
		String sundayDate = "";
		// Get calendar set to current date and time
		Calendar c = Calendar.getInstance();

		// Set the calendar to monday of the current week
		c.set(Calendar.WEEK_OF_YEAR, weekNb);
		c.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);

		// Print dates of the current week starting on Monday
		DateFormat df = DateFormat.getDateInstance(DateFormat.MEDIUM);
		mondayDate = df.format(c.getTime());
		for (int i = 0; i < 6; i++) {
			c.add(Calendar.DATE, 1);
		}
		sundayDate = df.format(c.getTime());
		return new String(mondayDate + " - " + sundayDate);
	}

	public JSONArray getPersons() {
		JSONArray personsOutput = new JSONArray();
		for (int j = 0; j < 4; j++) {

			JSONObject tmp = new JSONObject();
			tmp.put("Nom", service.Get(j).LastName);
			tmp.put("Prénom", service.Get(j).FirstName);
			tmp.put("Tel", service.Get(j).PhoneNumber);
			personsOutput.put(tmp);
		}
		return personsOutput;
	}

	public JSONObject getPerson(int index) {
		JSONObject personOutput = new JSONObject();

		personOutput.put("Nom", service.Get(index).LastName);
		personOutput.put("Prénom", service.Get(index).FirstName);
		personOutput.put("Tel", service.Get(index).PhoneNumber);

		return personOutput;
	}

	public void setCBLabel(int number, PersonService service) {
		this.CBLabel = new StringBuilder().append(getDateOfWeek(number)).append("   : ").append(service.nbInitialized())
				.append("/4").toString();
	}

	public String getCBLabel() {
		return CBLabel;
	}

}
