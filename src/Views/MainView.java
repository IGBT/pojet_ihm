package Views;

import java.awt.EventQueue;
import java.awt.GridBagConstraints;
//import javax.swing.DefaultComboBoxModel;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;

import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JSplitPane;

import org.json.JSONArray;
import org.json.JSONObject;

import Business.PersonService;
import DataModels.Person;

public class MainView extends JFrame implements ActionListener {

	private static final long serialVersionUID = 1L;

	private JFrame frame;
	private int MaxWeeksNumber;
	private JComboBox<String> cb;
	private String selectedWeek;
	private int cbIndex;
	private ArrayList<WeekView> weekViews;
	private ArrayList<String> weekNb;
	private JPanel panel;
	private String filename = "ArrosageData.json";
	private Path path = Paths.get(filename);
	private boolean exists = Files.exists(path);

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MainView window = new MainView();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Initialize the contents of the frame.
	 */
	public MainView() {
		cb = new JComboBox<String>();
		cb.addActionListener(this);
		MaxWeeksNumber = 52;

		weekViews = new ArrayList<WeekView>();
		weekNb = new ArrayList<String>();

		frame = new JFrame("Planning Arrosage");
		frame.setBounds(100, 100, 900, 450);
		frame.addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent e) {
				try {
					writeJson(filename);
				} catch (Exception ev) {
					ev.printStackTrace();
				}
				System.exit(0);
			}
		});
		GridBagLayout gridBagLayout = new GridBagLayout();
		gridBagLayout.columnWidths = new int[] { 434, 0 };
		gridBagLayout.rowHeights = new int[] { 261, 0 };
		gridBagLayout.columnWeights = new double[] { 1.0, Double.MIN_VALUE };
		gridBagLayout.rowWeights = new double[] { 1.0, Double.MIN_VALUE };
		frame.getContentPane().setLayout(gridBagLayout);

		JSplitPane splitPane = new JSplitPane();
		GridBagConstraints gbc_splitPane = new GridBagConstraints();
		gbc_splitPane.fill = GridBagConstraints.BOTH;
		gbc_splitPane.gridx = 0;
		gbc_splitPane.gridy = 0;
		frame.getContentPane().add(splitPane, gbc_splitPane);

		splitPane.setLeftComponent(cb); // comboBox avec les semaines

		panel = new JPanel();
		splitPane.setRightComponent(panel);

		if (exists) {
			readJson(path);
		} else {

			for (int i = 0; i < MaxWeeksNumber; i++) {
				WeekView weekView = new WeekView(i, new PersonService());
				weekViews.add(weekView);
				weekNb.add(weekView.getLabel());
				cb.addItem(weekView.getCBLabel());
			}
		}

	}

	public void actionPerformed(ActionEvent e) {
		if (cb != null && e.getActionCommand() == "comboBoxChanged") {
			cbIndex = cb.getSelectedIndex();

			panel.removeAll();
			panel.add(weekViews.get(cbIndex));
			panel.validate();
			panel.repaint();

		}
	}

	public String getSelectedWeek() {
		return selectedWeek;
	}

	public void setSelectedWeek(String selectedWeek) {
		this.selectedWeek = selectedWeek;
	}

	public void writeJson(String filename) throws Exception {
		if (exists) {
			Files.delete(path);
		}
		for (int i = 0; i < MaxWeeksNumber; i++) {
			JSONObject weekOutput = new JSONObject();
			weekOutput.put("label", weekViews.get(i).getLabel());
			weekOutput.put("Persons", weekViews.get(i).getPersons());
			Files.write(path, new StringBuilder().append(weekOutput.toString()).append("\n").toString().getBytes(),
					StandardOpenOption.CREATE, StandardOpenOption.APPEND);

		}
	}

	public void readJson(Path path) {
		try {
			BufferedReader reader = new BufferedReader(new FileReader(path.toString()));

			for (int k = 0; k < MaxWeeksNumber; k++) {
				JSONObject weekData = new JSONObject(reader.readLine());
				PersonService ps = new PersonService();

				JSONArray dataPersons = weekData.getJSONArray("Persons");
				for (int i = 0; i < dataPersons.length(); i++) {
					Person tmp = new Person(dataPersons.getJSONObject(i).getString("Prénom"),
							dataPersons.getJSONObject(i).getString("Nom"),
							dataPersons.getJSONObject(i).getString("Tel"));
					if (tmp.FirstName.isBlank()) {
						ps.Add(i, new Person());
					} else {
						ps.Add(i, tmp);
					}
				}
				WeekView wk = new WeekView(k, ps);
				weekViews.add(wk);
				weekNb.add(wk.getLabel());
				cb.addItem(wk.getCBLabel());
			}
			reader.close();

		} catch (IOException ex) {
			ex.printStackTrace();
		}

	}
}
