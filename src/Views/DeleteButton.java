package Views;

import javax.swing.ImageIcon;
import javax.swing.JButton;

public class DeleteButton extends JButton {
	private static final long serialVersionUID = 1L;
	final ImageIcon deleteIcon = new ImageIcon(getClass().getResource("/resources/deleteIcon.png"));

	public DeleteButton() {
		// setOpaque(false);
		setContentAreaFilled(false);
		setBorderPainted(false);
		setEnabled(false);
		setIcon(deleteIcon);

	}

}
