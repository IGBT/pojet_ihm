package DataModels;

public class Person {

	public String FirstName;
	public String LastName;
	public String PhoneNumber;
	public Boolean isInitialized;

	public Person() {
		FirstName = "";
		LastName = "";
		PhoneNumber = "";
		isInitialized = false;
	};

	public Person(String firstName, String lastName, String phoneNumber) {
		FirstName = firstName;
		LastName = lastName;
		PhoneNumber = phoneNumber;
		isInitialized = true;
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
	}

}
